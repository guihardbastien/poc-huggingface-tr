from transformers import AutoTokenizer, AutoModelForSequenceClassification
import torch
model_name = "MoritzLaurer/mDeBERTa-v3-base-mnli-xnli"
tokenizer = AutoTokenizer.from_pretrained(model_name)
model = AutoModelForSequenceClassification.from_pretrained(model_name)



premise = "cette vieille n'a pas dit bonjour et ça m'a énervé"
hypothesis = "l'émotion que je ressent"


input = tokenizer(premise, truncation=False, return_tensors="pt")
output = model(input["input_ids"].to("cpu"))  # device = "cuda:0" or "cpu"
prediction = torch.softmax(output["logits"][0], -1).tolist()

label_names = ["colère", "peur", "tristesse"]

prediction = {name: round(float(pred) * 100, 1) for pred, name in zip(prediction, label_names)}
print(prediction)