from transformers import pipeline
import csv
import statistics

classifier_fr = pipeline("zero-shot-classification",
                         model="BaptisteDoyen/camembert-base-xnli")

labels_for_printing = ["neutre", "colère", "peur", "tristesse", "joie"]
labels = ["colère", "peur", "tristesse", "joie"]
size = 5
switcher = {
    0: [1, 0, 0, 0, 0],
    1: [0, 1, 0, 0, 0],
    2: [0, 0, 1, 0, 0],
    3: [0, 0, 0, 1, 0],
    4: [0, 0, 0, 0, 1]
}

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


cr_arr = []
threshold = 0.3

def print_classification(arr):
    for i in range(4):
        if arr[i] == 1:
            print(labels_for_printing[i])

def int_to_emotion(emo_class):
    """
    """
    return switcher.get(emo_class)


def threshold_filter_old(arr):
    """converts output into one hot given a threshold
    """
    filtered_vals = []
    for i in arr:
        filtered_vals.append(1 if i > threshold else 0)
    # if no emotion reaches the threshold add 1 as neutral else 0 
    filtered_vals.insert(0,0) if 1 in filtered_vals else filtered_vals.insert(0,1)
    return filtered_vals


def threshold_filter(arr):
    """converts best candidate into one hot
    """
    one_hot_index = -1
    current_best_score = -1
    for i in range(len(arr)):
        val = arr[i]
        if(val > current_best_score):
            one_hot_index = i
            current_best_score = val
    arr = [0, 0, 0, 0, 0]
    arr[one_hot_index] = 1
    return arr


def sort_res(res):
    """ Makes sure the classifier response matches the initial order.
    We need to do that as the classfier runs each prediction in parallel
    """
    sorted_arr = [0 for i in range(len(labels))]
    scores = res["scores"]
    unsorted_labels = res["labels"]

    for i in range(len(labels)):
        index = labels.index(unsorted_labels[i])
        sorted_arr[index] = scores[i]
    return sorted_arr


mean_error_by_classification = []
correct_answers = []

with open('test_data.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=';')
    for row in csv_reader:
        cr = {}
        txt = row["txt"]
        cr["txt"] = txt
        cr["expected"] = int_to_emotion(int(row["emotion"]))

        hypothesis_template = "l'émotion de ce texte est {}."
        res = classifier_fr(txt, candidate_labels=labels, hypothesis_template=hypothesis_template)
        cr["actual"] = sort_res(res)


        # error
        deltas = []
        for i in range(len(labels)):
            deltas.append(abs(cr["expected"][i] - cr["actual"][i]))

        mean_error_by_classification.append(statistics.mean(deltas))

        # array of len 4
        classification = threshold_filter_old(cr["actual"])
        # array of len 5
        correct_answers.append(1 if classification == cr["expected"] else 0)
        if classification == cr["expected"]:
            print(f"{bcolors.OKGREEN}{txt}{bcolors.ENDC}")
        else:
            print(f"{bcolors.FAIL}{txt}{bcolors.ENDC}")
        print_classification(classification)
        #print(cr)

        cr_arr.append(cr)

print(statistics.mean(mean_error_by_classification))
print(sum(correct_answers), " / ", len(correct_answers))

inf = [0, 0, 0, 0, 0]
for c in cr_arr:
    inf = [x + y for x, y in zip(inf, c["actual"])]

print(list(zip(labels, inf)))
